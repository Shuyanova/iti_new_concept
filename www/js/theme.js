/**
 * Created by Squirrel on 24.08.15.
 */



jQuery(document).ready(function(){

    jQuery('.btn-menu').click(function() {
        jQuery('.sidebar').toggleClass('sidebar-open');
        jQuery('.main').toggleClass('main-offset');
        var sidebarWidth = $(".sidebar").width();
        if (sidebarWidth < 71) {
            $(".logo-small img").attr("src","images/logo-small.png");
        } else {
            $(".logo-small img").attr("src","images/logo.png");
        }
    });

});

var isFirefox = (navigator.userAgent.toLowerCase().indexOf('firefox') > -1);
var isMobile = (typeof window.orientation !== "undefined") ||
    (navigator.userAgent.indexOf('IEMobile') !== -1);

if(isFirefox && !isMobile) {
    $('a[href^="tel:"]').click(function() { return false; });
}






